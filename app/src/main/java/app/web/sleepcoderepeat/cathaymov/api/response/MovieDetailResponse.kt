package app.web.sleepcoderepeat.cathaymov.api.response

import app.web.sleepcoderepeat.cathaymov.api.dataclass.Genre
import app.web.sleepcoderepeat.cathaymov.api.dataclass.ProductionCompany
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import java.io.Serializable

data class MovieDetailResponse(
    val adult: Boolean?,
    val backdrop_path: String?,
    val belongs_to_collection: Any?,
    val budget: Int?,
    val genres: MutableList<Genre>?,
    val homepage: String?,
    val id: Int?,
    val imdb_id: String?,
    val original_language: String?,
    val original_title: String?,
    val overview: String?,
    val popularity: Double?,
    var poster_path: String?,
    val production_companies: MutableList<ProductionCompany>?,
    val release_date: String?,
    val revenue: Int?,
    val runtime: Int?,
    val spoken_languages: JsonArray?,
    val status: String?,
    val tagline: String?,
    val title: String?,
    val video: Boolean?,
    val vote_average: Double?,
    val vote_count: Int?,
    val videos: JsonObject?
) : Serializable