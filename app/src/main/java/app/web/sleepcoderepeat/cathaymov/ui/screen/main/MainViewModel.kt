package app.web.sleepcoderepeat.cathaymov.ui.screen.main

import android.util.Log
import androidx.databinding.ObservableArrayList
import androidx.databinding.ObservableList
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import app.web.sleepcoderepeat.cathaymov.api.dataclass.MovieItem
import app.web.sleepcoderepeat.cathaymov.api.response.MovieListResponse
import app.web.sleepcoderepeat.cathaymov.data.MainDataRemoteSource
import app.web.sleepcoderepeat.cathaymov.data.remote.MainRemoteSource
import app.web.sleepcoderepeat.cathaymov.databinding.MovieItemBinding
import app.web.sleepcoderepeat.cathaymov.utils.Constants.Companion.PAGE_KEY
import app.web.sleepcoderepeat.cathaymov.utils.Constants.Companion.PRIMARY_RELEASE_DATE_KEY
import app.web.sleepcoderepeat.cathaymov.utils.Constants.Companion.QUERY_KEY
import app.web.sleepcoderepeat.cathaymov.utils.Constants.Companion.RELEASE_DATE_DESC
import app.web.sleepcoderepeat.cathaymov.utils.Constants.Companion.SORT_BY_KEY
import app.web.sleepcoderepeat.cathaymov.utils.Constants.Companion.getSortByMap
import app.web.sleepcoderepeat.cathaymov.utils.SingleLiveEvent
import app.web.sleepcoderepeat.cathaymov.utils.format
import io.reactivex.disposables.CompositeDisposable
import java.util.*
import kotlin.collections.HashMap

class MainViewModel @ViewModelInject constructor(private val mainRemoteSource: MainRemoteSource) :
    ViewModel() {

    private val disposable: CompositeDisposable by lazy { CompositeDisposable() }

    val loading = MutableLiveData<Boolean>()
    val orderedBy = MutableLiveData<String>()
    val keyword = MutableLiveData<String>()
    val pageDiscover = MutableLiveData<String>()
    val pageSearch = MutableLiveData<String>()
    val currentlyShowingList: ObservableList<MovieItem> = ObservableArrayList()
    val discoverList: ObservableList<MovieItem> = ObservableArrayList()
    val searchResultList: ObservableList<MovieItem> = ObservableArrayList()
    internal val openDetailMovie = SingleLiveEvent<MovieClickObject>()

    fun init() {
        loading.value = false
        orderedBy.value = RELEASE_DATE_DESC
        pageDiscover.value = "1"
        pageSearch.value = "1"
        searchResultList.clear()
        discoverList.clear()
        getListDiscover()
    }

    fun getListDiscover() {
        loading.value = true
        disposable.add(mainRemoteSource.getDiscoverData(
            HashMap<String?, String?>().apply {
                put(PAGE_KEY, pageDiscover.value)
                put(PRIMARY_RELEASE_DATE_KEY, Calendar.getInstance().time.format())
                put(SORT_BY_KEY, orderedBy.value)
            },
            object : MainDataRemoteSource.GetMovieListCallback {
                override fun onSuccess(movieListResponse: MovieListResponse) {
                    with(discoverList) {
                        addAll(movieListResponse.movieItems)
                        Log.e(TAG, movieListResponse.toString())
                    }
                    loading.value = false
                }

                override fun onError(msg: String?) {
                    msg?.let {
                        Log.e(TAG, it)
                    }
                    loading.value = false
                }
            }
        ))
    }

    fun getCurrentlyShowing() {
        loading.value = true
        disposable.add(
            mainRemoteSource.getCurrentlyShowingData(
                HashMap<String?, String?>().apply {
                    put(PAGE_KEY, "1")
                },
                object : MainDataRemoteSource.GetMovieListCallback {
                    override fun onSuccess(movieListResponse: MovieListResponse) {
                        with(currentlyShowingList) {
                            currentlyShowingList.clear()
                            addAll(movieListResponse.movieItems)
                            Log.e(TAG, movieListResponse.toString())
                        }
                        loading.value = false
                    }

                    override fun onError(msg: String?) {
                        msg?.let {
                            Log.e(TAG, msg)
                        }
                        loading.value = false
                    }
                }
            ))
    }

    fun openDetailMovie(binding: MovieItemBinding?, movieId: Int) {
        openDetailMovie.value = MovieClickObject(binding, movieId)
    }

    private fun resetSearch() {
        pageSearch.value = "1"
        searchResultList.clear()
    }

    fun getSearchResult(action: String?) {
        if (keyword.value.isNullOrBlank()) {
            resetSearch()
            return
        }
        if (action.equals("init")) {
            resetSearch()
        }
        disposable.add(mainRemoteSource.getMoviesBySearchData(
            HashMap<String?, String?>().apply {
                put(PAGE_KEY, pageSearch.value)
                put(QUERY_KEY, keyword.value)
            },
            object : MainDataRemoteSource.GetMovieListCallback {
                override fun onSuccess(movieListResponse: MovieListResponse) {
                    if (movieListResponse.movieItems.size == 0 && action.equals("init")) {
                        resetSearch()
                        return
                    }
                    searchResultList.addAll(movieListResponse.movieItems)

                }

                override fun onError(msg: String?) {
                    resetSearch()

                }
            }
        ))
    }

    fun getValueSortBy(key: String): String? = getSortByMap()[key]

    override fun onCleared() {
        super.onCleared()
        disposable.clear()
    }

    companion object {
        private val TAG = MainViewModel::class.simpleName
    }

}