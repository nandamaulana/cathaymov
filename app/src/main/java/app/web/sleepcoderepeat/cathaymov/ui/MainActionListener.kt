package app.web.sleepcoderepeat.cathaymov.ui

interface MainActionListener {
    fun onItemClick()
}