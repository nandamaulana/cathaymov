package app.web.sleepcoderepeat.cathaymov.api

import app.web.sleepcoderepeat.cathaymov.api.response.CreditResponse
import app.web.sleepcoderepeat.cathaymov.api.response.MovieDetailResponse
import app.web.sleepcoderepeat.cathaymov.api.response.MovieListResponse
import app.web.sleepcoderepeat.cathaymov.api.response.PersonDetailResponse
import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.QueryMap

interface ApiService {

    @GET("movie/now_playing")
    fun getCurrentlyShowing(@QueryMap queries: HashMap<String?, String?>?): Observable<Response<MovieListResponse>>

    @GET("movie/popular")
    fun getPopular(@QueryMap queries: HashMap<String?, String?>?): Observable<Response<MovieListResponse>>

    @GET("movie/upcoming")
    fun getUpcoming(@QueryMap queries: HashMap<String?, String?>?): Observable<Response<MovieListResponse>>

    @GET("discover/movie")
    fun getDiscover(@QueryMap queries: HashMap<String?, String?>?): Observable<Response<MovieListResponse>>

    @GET("movie/{movie_id}")
    fun getMovieDetails(
        @Path("movie_id") id: Int,
        @QueryMap queries: HashMap<String?, String?>?
    ): Observable<Response<MovieDetailResponse>>

    @GET("movie/{movie_id}/credits")
    fun getCredit(
        @Path("movie_id") id: Int,
        @QueryMap queries: HashMap<String?, String?>?
    ): Observable<Response<CreditResponse>>

    @GET("person/{person_id}")
    fun getPersonDetails(
        @Path("person_id") id: Int,
        @QueryMap queries: HashMap<String?, String?>?
    ): Observable<Response<PersonDetailResponse>>

    @GET("search/movie")
    fun getMoviesBySearch(@QueryMap queries: HashMap<String?, String?>?): Observable<Response<MovieListResponse>>

}