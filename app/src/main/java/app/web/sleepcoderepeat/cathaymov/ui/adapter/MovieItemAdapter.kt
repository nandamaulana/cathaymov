package app.web.sleepcoderepeat.cathaymov.ui.adapter

import android.app.Activity
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import app.web.sleepcoderepeat.cathaymov.R
import app.web.sleepcoderepeat.cathaymov.api.dataclass.Genre
import app.web.sleepcoderepeat.cathaymov.api.dataclass.MovieItem
import app.web.sleepcoderepeat.cathaymov.databinding.MovieItemBinding
import app.web.sleepcoderepeat.cathaymov.ui.MainActionListener
import app.web.sleepcoderepeat.cathaymov.ui.screen.main.MainViewModel
import app.web.sleepcoderepeat.cathaymov.utils.Constants.Companion.getGenreMap
import app.web.sleepcoderepeat.cathaymov.utils.getSizeByScreenSize
import app.web.sleepcoderepeat.cathaymov.utils.toImageUrl

class MovieItemAdapter(
    private var movieItemList: MutableList<MovieItem>,
    private val mainViewModel: MainViewModel
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return MovieItemHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.movie_item,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        movieItemList[position].apply {
            (holder as MovieItemHolder).bind(this, object : MainActionListener {
                override fun onItemClick() {
                    id?.let {
                        mainViewModel.openDetailMovie(holder.binding, it)
                    }
                }
            })
        }
    }

    override fun getItemCount(): Int = movieItemList.size

    fun replaceData(movieList: MutableList<MovieItem>) {
        setList(movieList)
    }

    private fun setList(movieList: MutableList<MovieItem>) {
        this.movieItemList = movieList
        notifyDataSetChanged()
    }

    class MovieItemHolder(val binding: MovieItemBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(movieItem: MovieItem, mainActionListener: MainActionListener) {
            val item = movieItem.copy()
            movieItem.poster_path?.let {
                item.poster_path = movieItem.poster_path?.toImageUrl()
            }
            binding.movieImage.clipToOutline = true
            binding.movieItem = item
            val genreList: MutableList<Genre> = arrayListOf()
            val genreMap = getGenreMap()
            movieItem.genre_ids?.apply {
                map { i ->
                    genreMap?.let {
                        it[i]?.apply {
                            genreList.add(Genre(i, this))
                        }
                    }
                }
            }
            val dm = DisplayMetrics()
            (binding.root.context as Activity).windowManager.defaultDisplay.getMetrics(dm)
            val sized = dm.getSizeByScreenSize()
            binding.movieItemLayout.apply {
                layoutParams.width = sized
                layoutParams.height = sized + (sized / 2)
            }
            binding.rvMovieGenre.adapter = GenreItemAdapter(genreList)
            binding.action = mainActionListener
            binding.executePendingBindings()
        }

    }
}