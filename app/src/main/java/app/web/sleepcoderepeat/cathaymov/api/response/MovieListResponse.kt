package app.web.sleepcoderepeat.cathaymov.api.response

import android.os.Parcelable
import app.web.sleepcoderepeat.cathaymov.api.dataclass.MovieItem
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import java.io.Serializable


data class MovieListResponse(
    val page: Int,
    @SerializedName("results") val movieItems: MutableList<MovieItem>,
    val total_pages: Int,
    val total_results: Int
) : Serializable