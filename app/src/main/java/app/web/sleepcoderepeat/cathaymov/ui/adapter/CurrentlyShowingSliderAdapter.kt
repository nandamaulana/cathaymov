package app.web.sleepcoderepeat.cathaymov.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.widget.ViewPager2
import app.web.sleepcoderepeat.cathaymov.R
import app.web.sleepcoderepeat.cathaymov.api.dataclass.MovieItem
import app.web.sleepcoderepeat.cathaymov.databinding.CurrentlyShowingItemBinding
import app.web.sleepcoderepeat.cathaymov.ui.MainActionListener
import app.web.sleepcoderepeat.cathaymov.ui.screen.main.MainViewModel
import app.web.sleepcoderepeat.cathaymov.utils.toImageUrl

class CurrentlyShowingSliderAdapter(
    private var currentlyShowingList: MutableList<MovieItem>,
    private var mainViewModel: MainViewModel,
    private var viewPager2: ViewPager2
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return CurrentlyShowingHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.currently_showing_item,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        currentlyShowingList[position].let {
            (holder as CurrentlyShowingHolder).bind(it,
                object : MainActionListener {
                    override fun onItemClick() {
                        it.id?.apply {
                            mainViewModel.openDetailMovie(null, this)
                        }
                    }
                })
        }
    }

    override fun getItemCount(): Int = currentlyShowingList.size


    fun replaceData(items: MutableList<MovieItem>) {
        setList(items)
    }

    private fun setList(item: MutableList<MovieItem>) {
        this.currentlyShowingList = item
        notifyDataSetChanged()
    }

    class CurrentlyShowingHolder(var binding: CurrentlyShowingItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(movieItem: MovieItem, action: MainActionListener) {
            val item = movieItem.copy()
            movieItem.poster_path?.let {
                item.poster_path = movieItem.poster_path?.toImageUrl()
            }
            binding.action = action
            binding.movieImage.clipToOutline = true
            binding.movieItem = item

        }
    }
}