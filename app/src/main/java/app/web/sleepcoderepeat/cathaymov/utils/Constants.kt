package app.web.sleepcoderepeat.cathaymov.utils

import java.util.*
import kotlin.collections.HashMap

class Constants {
    companion object {
        const val BASE_URL = "https://api.themoviedb.org/3/"
        const val IMAGE_BASE_URL = "https://image.tmdb.org/t/p/original"
        const val IMAGE_BASE_URL_W500 = "https://image.tmdb.org/t/p/w500"
        const val BASE_CATHAY_URL = "https://www.cathaycineplexes.com.sg/"
        const val PACKAGE_CHROME = "com.android.chrome"

        const val API_KEY = "api_key"
        const val PRIMARY_RELEASE_DATE_KEY = "primary_release_date.lte"
        const val SORT_BY_KEY = "sort_by"
        const val RELEASE_DATE_DESC = "release_date.desc"
        const val RELEASE_DATE_ASC = "release_date.asc"
        const val PAGE_KEY = "page"
        const val QUERY_KEY = "query"
        const val APPEND_TO_RESPONSE = "append_to_response"

        const val MOVIE_ID_KEY = "movie_id"
        const val MOVIE_IMAGE_KEY = "movie_image"
        const val MOVIE_NAME_KEY = "movie_name"

        fun getSortByMap(): HashMap<String, String> {
            val sortByMap = HashMap<String, String>()
            sortByMap["popularity.asc"] = "popularity asc"
            sortByMap["popularity.desc"] = "popularity desc"
            sortByMap["release_date.asc"] = "release date asc"
            sortByMap["release_date.desc"] = "release date desc"
            sortByMap["revenue.asc"] = "revenue asc"
            sortByMap["revenue.desc"] = "revenue desc"
            sortByMap["primary_release_date.asc"] = "primary release date asc"
            sortByMap["primary_release_date.desc"] = "primary release date desc"
            sortByMap["original_title.asc"] = "original title asc"
            sortByMap["original_title.desc"] = "original title desc"
            sortByMap["vote_average.asc"] = "vote average asc"
            sortByMap["vote_average.desc"] = "vote average desc"
            sortByMap["vote_count.asc"] = "vote count asc"
            sortByMap["vote_count.desc"] = "vote count desc"
            return sortByMap
        }

        fun getGenreMap(): HashMap<Int, String>? {
            val genreMap = HashMap<Int, String>()
            genreMap[28] = "Action"
            genreMap[12] = "Adventure"
            genreMap[16] = "Animation"
            genreMap[35] = "Comedy"
            genreMap[80] = "Crime"
            genreMap[99] = "Documentary"
            genreMap[18] = "Drama"
            genreMap[10751] = "Family"
            genreMap[14] = "Fantasy"
            genreMap[36] = "History"
            genreMap[27] = "Horror"
            genreMap[10402] = "Music"
            genreMap[9648] = "Mystery"
            genreMap[10749] = "Romance"
            genreMap[878] = "Science Fiction"
            genreMap[53] = "Thriller"
            genreMap[10752] = "War"
            genreMap[37] = "Western"
            genreMap[10770] = "TV Movie"
            return genreMap
        }
    }
}