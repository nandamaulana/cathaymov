package app.web.sleepcoderepeat.cathaymov.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import app.web.sleepcoderepeat.cathaymov.R
import app.web.sleepcoderepeat.cathaymov.api.dataclass.Cast
import app.web.sleepcoderepeat.cathaymov.databinding.CastItemBinding
import app.web.sleepcoderepeat.cathaymov.utils.toImageUrl

class CastAdapter(private var castList: MutableList<Cast>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return CastItemHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.cast_item,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as CastItemHolder).bind(castList[position])
    }

    override fun getItemCount(): Int = castList.size

    fun replaceData(castList: MutableList<Cast>) {
        setList(castList)
    }

    private fun setList(castList: MutableList<Cast>) {
        this.castList = castList
        notifyDataSetChanged()
    }

    class CastItemHolder(private var binding: CastItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(castItem: Cast) {
            val item = castItem.copy()
            castItem.profile_path?.let {
                item.profile_path = castItem.profile_path?.toImageUrl()
            }
            binding.castItem = item
            binding.castImage.clipToOutline = true
            binding.executePendingBindings()
        }
    }
}