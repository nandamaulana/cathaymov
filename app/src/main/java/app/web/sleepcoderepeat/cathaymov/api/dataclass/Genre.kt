package app.web.sleepcoderepeat.cathaymov.api.dataclass

import java.io.Serializable

data class Genre(
    val id: Int?,
    val name: String?
) : Serializable