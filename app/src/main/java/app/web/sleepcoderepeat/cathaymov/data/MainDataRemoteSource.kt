package app.web.sleepcoderepeat.cathaymov.data

import app.web.sleepcoderepeat.cathaymov.api.response.CreditResponse
import app.web.sleepcoderepeat.cathaymov.api.response.MovieDetailResponse
import app.web.sleepcoderepeat.cathaymov.api.response.MovieListResponse
import app.web.sleepcoderepeat.cathaymov.api.response.PersonDetailResponse
import io.reactivex.disposables.Disposable

interface MainDataRemoteSource {
    fun getCurrentlyShowingData(
        map: HashMap<String?, String?>,
        callback: GetMovieListCallback
    ): Disposable

    fun getPopularData(
        map: HashMap<String?, String?>,
        callback: GetMovieListCallback
    ): Disposable

    fun getUpcomingData(
        map: HashMap<String?, String?>,
        callback: GetMovieListCallback
    ): Disposable

    fun getDiscoverData(
        map: HashMap<String?, String?>,
        callback: GetMovieListCallback
    ): Disposable

    fun getMovieDetailsData(
        movieId: Int,
        map: HashMap<String?, String?>,
        callback: GetMovieDetailCallback
    ): Disposable

    fun getCreditsData(
        movieId: Int,
        map: HashMap<String?, String?>,
        callback: GetCreditsCallback
    ): Disposable

    fun getPersonDetailsData(
        personId: Int,
        map: HashMap<String?, String?>,
        callback: GetPersonDetailCallback
    ): Disposable

    fun getMoviesBySearchData(
        map: HashMap<String?, String?>,
        callback: GetMovieListCallback
    ): Disposable

    interface GetMovieListCallback {
        fun onSuccess(movieListResponse: MovieListResponse)
        fun onError(msg: String?)
    }

    interface GetMovieDetailCallback {
        fun onSuccess(movieDetailResponse: MovieDetailResponse)
        fun onError(msg: String?)
    }

    interface GetCreditsCallback {
        fun onSuccess(creditResponse: CreditResponse)
        fun onError(msg: String?)
    }

    interface GetPersonDetailCallback {
        fun onSuccess(personDetailResponse: PersonDetailResponse)
        fun onError(msg: String?)
    }
}