package app.web.sleepcoderepeat.cathaymov.ui.screen.detail

import android.util.Log
import androidx.databinding.ObservableArrayList
import androidx.databinding.ObservableField
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import app.web.sleepcoderepeat.cathaymov.api.dataclass.Cast
import app.web.sleepcoderepeat.cathaymov.api.dataclass.Genre
import app.web.sleepcoderepeat.cathaymov.api.response.CreditResponse
import app.web.sleepcoderepeat.cathaymov.api.response.MovieDetailResponse
import app.web.sleepcoderepeat.cathaymov.data.MainDataRemoteSource
import app.web.sleepcoderepeat.cathaymov.data.remote.MainRemoteSource
import app.web.sleepcoderepeat.cathaymov.utils.Constants.Companion.APPEND_TO_RESPONSE
import app.web.sleepcoderepeat.cathaymov.utils.getRuntimeFormatted
import app.web.sleepcoderepeat.cathaymov.utils.toImageUrl
import com.google.gson.JsonArray
import io.reactivex.disposables.CompositeDisposable

class DetailMovieViewModel @ViewModelInject constructor(private val mainRemoteSource: MainRemoteSource) :
    ViewModel() {

    private val disposable: CompositeDisposable by lazy { CompositeDisposable() }

    val loading = MutableLiveData<Boolean>()
    val movieDetail: ObservableField<MovieDetailResponse> = ObservableField()
    val genreList: ObservableArrayList<Genre> = ObservableArrayList()
    val castList: ObservableArrayList<Cast> = ObservableArrayList()

    fun init(movieId: Int) {
        loading.value = false
        getDetailMovie(movieId)
        getCast(movieId)
    }

    fun getSpokenLanguages(spokenLanguages: JsonArray?): String {
        var result = ""
        spokenLanguages?.forEach { it ->
            result += it.asJsonObject.get("name").asString + ", "
        }
        return "Language : " + result.dropLast(2)
    }

    fun getRuntimeFormatted(runtime: Int): String {
        return runtime.getRuntimeFormatted()
    }

    private fun getDetailMovie(movieId: Int) {
        loading.value = true
        disposable.add(mainRemoteSource.getMovieDetailsData(
            movieId,
            HashMap<String?, String?>().apply {
                put(APPEND_TO_RESPONSE, "videos")
            },
            object : MainDataRemoteSource.GetMovieDetailCallback {
                override fun onSuccess(movieDetailResponse: MovieDetailResponse) {
                    movieDetailResponse.poster_path = movieDetailResponse.poster_path?.toImageUrl()
                    movieDetail.set(movieDetailResponse)
                    movieDetailResponse.genres?.let {
                        genreList.clear()
                        genreList.addAll(it)
                    }
                    loading.value = false
                    Log.e(TAG, movieDetail.get().toString())
                }

                override fun onError(msg: String?) {
                    msg?.let {
                        Log.e(TAG, it)
                    }
                    loading.value = false
                }
            }
        ))
    }

    private fun getCast(movieId: Int) {
        loading.value = true
        disposable.add(mainRemoteSource.getCreditsData(
            movieId,
            HashMap(),
            object : MainDataRemoteSource.GetCreditsCallback {
                override fun onSuccess(creditResponse: CreditResponse) {
                    castList.addAll(creditResponse.cast)
                }

                override fun onError(msg: String?) {
                    msg?.let {
                        Log.e(TAG, it)
                    }
                }
            }
        ))
    }

    override fun onCleared() {
        super.onCleared()
        disposable.clear()
    }

    companion object {
        private val TAG = DetailMovieViewModel::class.simpleName
    }

}