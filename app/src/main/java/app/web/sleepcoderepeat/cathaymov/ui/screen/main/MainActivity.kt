package app.web.sleepcoderepeat.cathaymov.ui.screen.main

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityOptionsCompat
import androidx.core.util.Pair
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.widget.CompositePageTransformer
import androidx.viewpager2.widget.MarginPageTransformer
import androidx.viewpager2.widget.ViewPager2
import app.web.sleepcoderepeat.cathaymov.R
import app.web.sleepcoderepeat.cathaymov.databinding.ActivityMainBinding
import app.web.sleepcoderepeat.cathaymov.ui.adapter.CurrentlyShowingSliderAdapter
import app.web.sleepcoderepeat.cathaymov.ui.adapter.MovieItemAdapter
import app.web.sleepcoderepeat.cathaymov.ui.screen.detail.DetailMovieActivity
import app.web.sleepcoderepeat.cathaymov.utils.Constants.Companion.MOVIE_ID_KEY
import app.web.sleepcoderepeat.cathaymov.utils.Constants.Companion.MOVIE_IMAGE_KEY
import app.web.sleepcoderepeat.cathaymov.utils.Constants.Companion.MOVIE_NAME_KEY
import app.web.sleepcoderepeat.cathaymov.utils.obtainViewModel
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint
import kotlin.math.abs

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    lateinit var mainBinding: ActivityMainBinding
    private lateinit var currentlyShowingSliderAdapter: CurrentlyShowingSliderAdapter
    private val mHandler = Handler()
    private var sliderHandler: Handler = Handler()
    private var sliderRunnable: Runnable = Runnable {
        val sliderBinding = mainBinding.vpCurrentlyShowingSlider
        if (sliderBinding.currentItem == currentlyShowingSliderAdapter.itemCount - 1) {
            sliderBinding.currentItem = 0
        } else {
            sliderBinding.currentItem =
                sliderBinding.currentItem + 1
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mainBinding = ActivityMainBinding.inflate(layoutInflater).apply {
            vm = obtainViewModel(MainViewModel::class.java)
        }
        setContentView(mainBinding.root)
        mainBinding.lifecycleOwner = this
        setUpCurrentlyShowingSlider()
        setUpObserver()
        setUpBinding()
        setUpListener()
    }

    private fun setUpCurrentlyShowingSlider() {
        mainBinding.vm?.apply {
            with(mainBinding.vpCurrentlyShowingSlider) {
                currentlyShowingSliderAdapter =
                    CurrentlyShowingSliderAdapter(currentlyShowingList, this@apply, this)
                adapter = currentlyShowingSliderAdapter
                clipToPadding = false
                clipChildren = false
                offscreenPageLimit = 5
            }
        }
        val compositePageTransformer = CompositePageTransformer()
        with(compositePageTransformer) {
            addTransformer(MarginPageTransformer(40))
            addTransformer { page, position ->
                val r: Float = 1 - abs(position)
                page.scaleY = (0.85f + r * 0.15f)
            }
        }
        with(mainBinding.vpCurrentlyShowingSlider) {
            setPageTransformer(compositePageTransformer)
            registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
                override fun onPageSelected(position: Int) {
                    super.onPageSelected(position)
                    sliderHandler.removeCallbacks(sliderRunnable)
                    sliderHandler.postDelayed(sliderRunnable, 3000)
                }
            })
        }
    }

    private fun setUpObserver() {
        mainBinding.vm?.apply {
            openDetailMovie.observe(this@MainActivity, {
                val intent = Intent(this@MainActivity, DetailMovieActivity::class.java)
                intent.putExtra(MOVIE_ID_KEY, it.movieId)
                it.binding?.let { binding ->
                    val option = ActivityOptionsCompat.makeSceneTransitionAnimation(
                        this@MainActivity,
                        Pair.create(binding.movieImage, MOVIE_IMAGE_KEY),
                        Pair.create(binding.movieName, MOVIE_NAME_KEY)
                    ).toBundle()
                    startActivity(intent, option)
                    return@observe
                }
                startActivity(intent)
            })
            keyword.observe(this@MainActivity, {
                mHandler.removeCallbacksAndMessages(null)
                mHandler.postDelayed({
                    mainBinding.vm?.getSearchResult("init")
                }, 500)
            })
            init()
            getCurrentlyShowing()
        }
    }

    private fun setUpBinding() {
        mainBinding.vm?.let {
            with(mainBinding.rvDiscover) {
                adapter = MovieItemAdapter(it.discoverList, it)
                addOnScrollListener(discoverScrollListener())
            }
            with(mainBinding.rvSearchResult) {
                adapter = MovieItemAdapter(it.searchResultList, it)
                addOnScrollListener(searchScrollListener())
            }
        }
    }

    private fun setUpListener() {
        with(mainBinding.srlDiscover) {
            setOnRefreshListener {
                mainBinding.vm?.init()
            }
        }
    }


    private fun discoverScrollListener() = object : RecyclerView.OnScrollListener() {
        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
            super.onScrolled(recyclerView, dx, dy)
            mainBinding.vm?.loading?.value?.let {
                if (it) {
                    return
                }
            }
            if (!recyclerView.canScrollVertically(1)) {
                mainBinding.vm?.pageDiscover?.value?.apply {
                    mainBinding.vm?.pageDiscover?.value =
                        (mainBinding.vm?.pageDiscover?.value?.toInt()?.plus(1)).toString()
                }
                mainBinding.vm?.getListDiscover()
                Snackbar.make(mainBinding.clMain, R.string.load_more, Snackbar.LENGTH_LONG).apply {
                    animationMode = Snackbar.ANIMATION_MODE_SLIDE
                    setAction("Hide") {
                        this.dismiss()
                    }.show()
                }
            }
        }
    }

    private fun searchScrollListener() = object : RecyclerView.OnScrollListener() {
        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
            super.onScrolled(recyclerView, dx, dy)
            mainBinding.vm?.loading?.value?.let {
                if (it) {
                    return
                }
            }
            if (!recyclerView.canScrollVertically(1)) {
                mainBinding.vm?.pageSearch?.value?.apply {
                    mainBinding.vm?.pageSearch?.value =
                        (mainBinding.vm?.pageSearch?.value?.toInt()?.plus(1)).toString()
                }
                mainBinding.vm?.getSearchResult("scroll")
                Snackbar.make(mainBinding.clMain, R.string.load_more, Snackbar.LENGTH_LONG).apply {
                    animationMode = Snackbar.ANIMATION_MODE_SLIDE
                    setAction("Hide") {
                        this.dismiss()
                    }.show()
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
    }
}
