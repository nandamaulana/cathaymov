package app.web.sleepcoderepeat.cathaymov.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import app.web.sleepcoderepeat.cathaymov.R
import app.web.sleepcoderepeat.cathaymov.api.dataclass.Genre
import app.web.sleepcoderepeat.cathaymov.databinding.GenreItemBinding

class GenreItemAdapter(
    private var genreList: MutableList<Genre>
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return GenreItemHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.genre_item,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as GenreItemHolder).bind(genreList[position])
    }

    override fun getItemCount(): Int = if(genreList.size > 4) 4 else genreList.size

    fun replaceData(genreList: MutableList<Genre>) {
        setList(genreList)
    }

    private fun setList(genreList: MutableList<Genre>) {
        this.genreList = genreList
        notifyDataSetChanged()
    }

    class GenreItemHolder(private var binding: GenreItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(genre: Genre) {
            binding.genre = genre
            binding.executePendingBindings()
        }
    }
}