package app.web.sleepcoderepeat.cathaymov.ui

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import androidx.viewpager2.widget.ViewPager2
import app.web.sleepcoderepeat.cathaymov.api.dataclass.Cast
import app.web.sleepcoderepeat.cathaymov.api.dataclass.Genre
import app.web.sleepcoderepeat.cathaymov.api.dataclass.MovieItem
import app.web.sleepcoderepeat.cathaymov.ui.adapter.CastAdapter
import app.web.sleepcoderepeat.cathaymov.ui.adapter.CurrentlyShowingSliderAdapter
import app.web.sleepcoderepeat.cathaymov.ui.adapter.GenreItemAdapter
import app.web.sleepcoderepeat.cathaymov.ui.adapter.MovieItemAdapter
import app.web.sleepcoderepeat.cathaymov.utils.load

object GlobalBinding {

    @BindingAdapter("loadImage")
    @JvmStatic
    fun setImage(imageView: ImageView, url: String?) {
        url?.let {
            imageView.load(it)
        }
    }

    @BindingAdapter("genreList")
    @JvmStatic
    fun setGenreList(recyclerView: RecyclerView, dataList: MutableList<Genre>?) {
        dataList?.let {
            (recyclerView.adapter as GenreItemAdapter).replaceData(it)
        }
    }

    @BindingAdapter("movieList")
    @JvmStatic
    fun setMovieList(recyclerView: RecyclerView, dataList: MutableList<MovieItem>) {
        if (dataList.size != 0)
            (recyclerView.adapter as MovieItemAdapter).replaceData(dataList)
    }

    @BindingAdapter("isRefreshing")
    @JvmStatic
    fun setRefreshingSwipeLayout(swipeRefreshLayout: SwipeRefreshLayout, refresh: Boolean) {
        swipeRefreshLayout.isRefreshing = refresh
    }

    @BindingAdapter("pagerAdapter")
    @JvmStatic
    fun setPagerAdapter(viewPager2: ViewPager2, items: MutableList<MovieItem>) {
        if (items.size != 0)
            (viewPager2.adapter as CurrentlyShowingSliderAdapter).replaceData(items)
    }


    @BindingAdapter("castList")
    @JvmStatic
    fun setCastList(recyclerView: RecyclerView, dataList: MutableList<Cast>) {
        if (dataList.size != 0)
            (recyclerView.adapter as CastAdapter).replaceData(dataList)
    }
}