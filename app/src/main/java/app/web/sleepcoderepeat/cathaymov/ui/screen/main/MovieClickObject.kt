package app.web.sleepcoderepeat.cathaymov.ui.screen.main

import app.web.sleepcoderepeat.cathaymov.databinding.MovieItemBinding

data class MovieClickObject(var binding: MovieItemBinding?, var movieId: Int)