package app.web.sleepcoderepeat.cathaymov.ui.screen.detail

import android.net.Uri
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.browser.customtabs.CustomTabsIntent
import app.web.sleepcoderepeat.cathaymov.databinding.ActivityDetailMovieBinding
import app.web.sleepcoderepeat.cathaymov.ui.adapter.CastAdapter
import app.web.sleepcoderepeat.cathaymov.ui.adapter.GenreItemAdapter
import app.web.sleepcoderepeat.cathaymov.ui.dialog.VideoDialog
import app.web.sleepcoderepeat.cathaymov.utils.Constants.Companion.BASE_CATHAY_URL
import app.web.sleepcoderepeat.cathaymov.utils.Constants.Companion.MOVIE_ID_KEY
import app.web.sleepcoderepeat.cathaymov.utils.Constants.Companion.PACKAGE_CHROME
import app.web.sleepcoderepeat.cathaymov.utils.obtainViewModel
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class DetailMovieActivity : AppCompatActivity() {

    private lateinit var detailMovieBinding: ActivityDetailMovieBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        detailMovieBinding = ActivityDetailMovieBinding.inflate(layoutInflater).apply {
            vm = obtainViewModel(DetailMovieViewModel::class.java)
        }
        setContentView(detailMovieBinding.root)
        detailMovieBinding.lifecycleOwner = this
        detailMovieBinding.moviePoster.clipToOutline = true
        setUpViewModel()
        setRecyclerView()
        setUpListener()
    }

    private fun setUpViewModel() {
        detailMovieBinding.vm?.init(intent.getIntExtra(MOVIE_ID_KEY, 0))
    }

    private fun setRecyclerView() {
        detailMovieBinding.vm?.let {
            detailMovieBinding.rvMovieGenre.adapter = GenreItemAdapter(it.genreList)
            detailMovieBinding.rvCast.adapter = CastAdapter(it.castList)
        }
    }

    private fun setUpListener() {
        detailMovieBinding.btnBookNow.setOnClickListener {
            CustomTabsIntent.Builder().build().apply {
                intent.setPackage(PACKAGE_CHROME)
                launchUrl(this@DetailMovieActivity, Uri.parse(BASE_CATHAY_URL))
            }
        }
        detailMovieBinding.playTrailer.setOnClickListener {
            detailMovieBinding.vm?.movieDetail?.get()?.videos?.let {
                if(it.getAsJsonArray("results").size() == 0) return@let 
                it.getAsJsonArray("results")?.get(0)?.asJsonObject?.get("key")?.asString?.apply {
                    VideoDialog(this).show(supportFragmentManager, "Video Dialog")
                }
                return@setOnClickListener
            }
            Toast.makeText(this, "Sorry trailer not found!", Toast.LENGTH_SHORT).show()
        }
    }


}