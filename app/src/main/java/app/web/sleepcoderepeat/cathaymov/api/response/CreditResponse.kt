package app.web.sleepcoderepeat.cathaymov.api.response

import app.web.sleepcoderepeat.cathaymov.api.dataclass.Cast
import app.web.sleepcoderepeat.cathaymov.api.dataclass.Crew

data class CreditResponse(
    val cast: MutableList<Cast>,
    val crew: MutableList<Crew>,
    val id: Int
)