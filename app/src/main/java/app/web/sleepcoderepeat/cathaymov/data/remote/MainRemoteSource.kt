package app.web.sleepcoderepeat.cathaymov.data.remote

import app.web.sleepcoderepeat.cathaymov.BuildConfig
import app.web.sleepcoderepeat.cathaymov.api.ApiService
import app.web.sleepcoderepeat.cathaymov.data.MainDataRemoteSource
import app.web.sleepcoderepeat.cathaymov.utils.Constants
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class MainRemoteSource @Inject constructor(private val apiService: ApiService) :
    MainDataRemoteSource {

    override fun getCurrentlyShowingData(
        map: HashMap<String?, String?>,
        callback: MainDataRemoteSource.GetMovieListCallback
    ): Disposable =
        apiService.getCurrentlyShowing(map.apply { put(Constants.API_KEY, BuildConfig.API_KEY) })
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe({
                if (it.code() != 200) {
                    callback.onError(it.errorBody().toString())
                } else {
                    it.body()?.apply {
                        callback.onSuccess(this)
                    }
                }
            }, {
                callback.onError(it.localizedMessage)
            })


    override fun getPopularData(
        map: HashMap<String?, String?>,
        callback: MainDataRemoteSource.GetMovieListCallback
    ): Disposable = apiService.getPopular(map.apply { put(Constants.API_KEY, BuildConfig.API_KEY) })
        .observeOn(AndroidSchedulers.mainThread())
        .subscribeOn(Schedulers.io())
        .subscribe({
            if (it.code() != 200) {
                callback.onError(it.errorBody().toString())
            } else {
                it.body()?.apply {
                    callback.onSuccess(this)
                }
            }
        }, {
            callback.onError(it.localizedMessage)
        })

    override fun getUpcomingData(
        map: HashMap<String?, String?>,
        callback: MainDataRemoteSource.GetMovieListCallback
    ): Disposable = apiService.getUpcoming(map.apply { put(Constants.API_KEY, BuildConfig.API_KEY) })
        .observeOn(AndroidSchedulers.mainThread())
        .subscribeOn(Schedulers.io())
        .subscribe({
            if (it.code() != 200) {
                callback.onError(it.errorBody().toString())
            } else {
                it.body()?.apply {
                    callback.onSuccess(this)
                }
            }
        }, {
            callback.onError(it.localizedMessage)
        })

    override fun getDiscoverData(
        map: HashMap<String?, String?>,
        callback: MainDataRemoteSource.GetMovieListCallback,
    ): Disposable = apiService.getDiscover(map.apply { put(Constants.API_KEY, BuildConfig.API_KEY) })
        .observeOn(AndroidSchedulers.mainThread())
        .subscribeOn(Schedulers.io())
        .subscribe({
            if (it.code() != 200){
                callback.onError(it.errorBody().toString())
            }else
                it.body()?.apply {
                    callback.onSuccess(this)
                }
        },{
            callback.onError(it.localizedMessage)
        })

    override fun getMovieDetailsData(
        movieId: Int,
        map: HashMap<String?, String?>,
        callback: MainDataRemoteSource.GetMovieDetailCallback
    ): Disposable = apiService.getMovieDetails(movieId,map.apply { put(Constants.API_KEY, BuildConfig.API_KEY) })
        .observeOn(AndroidSchedulers.mainThread())
        .subscribeOn(Schedulers.io())
        .subscribe({
            if (it.code() != 200) {
                callback.onError(it.errorBody().toString())
            } else {
                it.body()?.apply {
                    callback.onSuccess(this)
                }
            }
        }, {
            callback.onError(it.localizedMessage)
        })

    override fun getCreditsData(
        movieId: Int,
        map: HashMap<String?, String?>,
        callback: MainDataRemoteSource.GetCreditsCallback
    ): Disposable = apiService.getCredit(movieId, map.apply { put(Constants.API_KEY, BuildConfig.API_KEY) })
        .observeOn(AndroidSchedulers.mainThread())
        .subscribeOn(Schedulers.io())
        .subscribe({
            if (it.code() != 200) {
                callback.onError(it.errorBody().toString())
            } else {
                it.body()?.apply {
                    callback.onSuccess(this)
                }
            }
        }, {
            callback.onError(it.localizedMessage)
        })

    override fun getPersonDetailsData(
        personId: Int,
        map: HashMap<String?, String?>,
        callback: MainDataRemoteSource.GetPersonDetailCallback
    ): Disposable = apiService.getPersonDetails(personId, map.apply { put(Constants.API_KEY, BuildConfig.API_KEY) })
        .observeOn(AndroidSchedulers.mainThread())
        .subscribeOn(Schedulers.io())
        .subscribe({
            if (it.code() != 200) {
                callback.onError(it.errorBody().toString())
            } else {
                it.body()?.apply {
                    callback.onSuccess(this)
                }
            }
        }, {
            callback.onError(it.localizedMessage)
        })

    override fun getMoviesBySearchData(
        map: HashMap<String?, String?>,
        callback: MainDataRemoteSource.GetMovieListCallback
    ): Disposable = apiService.getMoviesBySearch(map.apply { put(Constants.API_KEY, BuildConfig.API_KEY) })
        .observeOn(AndroidSchedulers.mainThread())
        .subscribeOn(Schedulers.io())
        .subscribe({
            if (it.code() != 200) {
                callback.onError(it.errorBody().toString())
            } else {
                it.body()?.apply {
                    callback.onSuccess(this)
                }
            }
        }, {
            callback.onError(it.localizedMessage)
        })
}