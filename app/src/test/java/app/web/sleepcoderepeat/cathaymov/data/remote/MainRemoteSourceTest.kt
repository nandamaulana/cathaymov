package app.web.sleepcoderepeat.cathaymov.data.remote

import android.os.Build
import app.web.sleepcoderepeat.cathaymov.BuildConfig
import app.web.sleepcoderepeat.cathaymov.api.ApiService
import app.web.sleepcoderepeat.cathaymov.utils.Constants
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import dagger.hilt.android.testing.HiltTestApplication
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config
import javax.inject.Inject

@HiltAndroidTest
@RunWith(RobolectricTestRunner::class)
@Config(sdk = [Build.VERSION_CODES.P], application = HiltTestApplication::class)
class MainRemoteSourceTest {

    @get:Rule
    var hiltRule = HiltAndroidRule(this)

    @Inject
    lateinit var apiService: ApiService

    private val sampleFilm = 613504
    private val samplePerson = 122503
    private val sampleQuery = "mulan"

    @Before
    fun init() {
        hiltRule.inject()
    }

    @Test
    fun getCurrentlyShowingData() {
        try {
            val call = apiService.getCurrentlyShowing(
                HashMap<String?, String?>().apply {
                    put(Constants.API_KEY, BuildConfig.API_KEY)
                    put(Constants.PAGE_KEY, "1")
                }).blockingSingle()
            assertTrue(call.code() == 200)

            println("Good : ${call.body()}")
        } catch (e: Exception) {
            println("Error : ${e.message}")
            e.printStackTrace()
        }
    }

    @Test
    fun getPopularData() {
        try {
            val call = apiService.getPopular(
                HashMap<String?, String?>().apply {
                    put(Constants.API_KEY, BuildConfig.API_KEY)
                    put(Constants.PAGE_KEY, "1")
                }).blockingSingle()
            assertTrue(call.code() == 200)

            println("Good : ${call.body()}")
        } catch (e: Exception) {
            println("Error : ${e.message}")
            e.printStackTrace()
        }
    }

    @Test
    fun getUpcomingData() {
        try {
            val call = apiService.getUpcoming(
                HashMap<String?, String?>().apply {
                    put(Constants.API_KEY, BuildConfig.API_KEY)
                    put(Constants.PAGE_KEY, "1")
                }).blockingSingle()
            assertTrue(call.code() == 200)

            println("Good : ${call.body()}")
        } catch (e: Exception) {
            println("Error : ${e.message}")
            e.printStackTrace()
        }
    }

    @Test
    fun getDiscover() {
        try {
            val call = apiService.getDiscover(
                HashMap<String?, String?>().apply {
                    put(Constants.API_KEY, BuildConfig.API_KEY)
                    put(Constants.PAGE_KEY, "1")
                }).blockingSingle()
            assertTrue(call.code() == 200)

            println("Good : ${call.body()}")
        } catch (e: Exception) {
            println("Error : ${e.message}")
            e.printStackTrace()
        }
    }

    @Test
    fun getMovieDetailsData() {
        try {
            val call = apiService.getMovieDetails(
                sampleFilm,
                HashMap<String?, String?>().apply {
                    put(Constants.API_KEY, BuildConfig.API_KEY)
                    put(Constants.PAGE_KEY, "1")
                }).blockingSingle()
            assertTrue(call.code() == 200)

            println("Good : ${call.body()}")
        } catch (e: Exception) {
            println("Error : ${e.message}")
            e.printStackTrace()
        }
    }

    @Test
    fun getCreditsData() {
        try {
            val call = apiService.getCredit(
                sampleFilm,
                HashMap<String?, String?>().apply {
                    put(Constants.API_KEY, BuildConfig.API_KEY)
                    put(Constants.PAGE_KEY, "1")
                }).blockingSingle()
            assertTrue(call.code() == 200)

            println("Good : ${call.body()}")
        } catch (e: Exception) {
            println("Error : ${e.message}")
            e.printStackTrace()
        }
    }

    @Test
    fun getPersonDetailsData() {
        try {
            val call = apiService.getPersonDetails(
                samplePerson,
                HashMap<String?, String?>().apply {
                    put(Constants.API_KEY, BuildConfig.API_KEY)
                }).blockingSingle()
            println("Good : $call")
            assertTrue(call.code() == 200)

            println("Good : ${call.body()}")
        } catch (e: Exception) {
            println("Error : ${e.message}")
            e.printStackTrace()
        }
    }

    @Test
    fun getMoviesBySearchData() {
        try {
            val call = apiService.getMoviesBySearch(
                HashMap<String?, String?>().apply {
                    put(Constants.API_KEY, BuildConfig.API_KEY)
                    put(Constants.QUERY_KEY, sampleQuery)
                    put(Constants.PAGE_KEY, "1")
                }).blockingSingle()
            assertTrue(call.code() == 200)

            println("Good : ${call.body()}")
        } catch (e: Exception) {
            println("Error : ${e.message}")
            e.printStackTrace()
        }
    }


}