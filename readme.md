# Cathay Mov (TMDB API)

This is a aplication to help user discover any movie

## Screen
| Home | Detail|
| ------------- |:-------------:|
| ![Home](https://i.ibb.co/yQXRvqZ/photo-2020-10-14-10-40-40.jpg) | ![Detail](https://i.ibb.co/P1ys0xz/photo-2020-10-14-10-40-47.jpg) |

## Main feature
- Now Playing
- Discover with infinity scroll
- Search with infity scroll
- Detail with casts
- Watch trailer of movies
- Book button open [Cathay Cineplex](cathaycineplexes.com.sg) in webview using customtabs
- dark theme that identic with cinemas

### Project Structure

This project is build using MVVM + Repository pattern (MainDataSource)
Put the api at `gradle.properties`
and I used API like this

`https://api.themoviedb.org/3/discover/movie?api_key=<api_key>&primary_release_date.lte=<current_date>&page=1&sort_by=release_date.desc`

For testing I use robolectric and hilt testing for test ApiService Request

### Folder
- api (contains ApiService, data class response & by item)
- data (contains MainRemoteSource provide resource for app )
- di (contains modul using hilt Provides ApiService Injection)
- ui (contains all UI layer such as screen, dialog, adapter, binding)
- utils (contains class that support project)

### Libraries
- [flexbox](https://github.com/google/flexbox-layout)
- [retrofit2 (using reactivex)](https://github.com/square/retrofit/tree/master/retrofit-adapters/rxjava2)
- [stetho](https://github.com/facebook/stetho)
- [okhttp](https://github.com/square/okhttp)
- [glide](https://github.com/bumptech/glide)
- [hilt + hilt viewmodel](https://developer.android.com/training/dependency-injection/hilt-android?hl=id)
- [androidyoutubeplayer](https://github.com/PierfrancescoSoffritti/android-youtube-player)
- [robolectric for testing](http://robolectric.org/getting-started/#building-with-android-studiogradle)
- [Custom Tabs browser](https://developer.android.com/jetpack/androidx/releases/browser)

### Future improvement
- Favorite using room
- Localization
- Upcoming movies
- popular movie
- cast detail with related movies
- more testing

# Author
Nanda Rizka Maulana

## Note
That's all I can make for this recruitment session I hope this project satisfied enough for reviewer